﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;

namespace TSoft.DominantColors
{

    public class DominantColor
    {
        public List<Color> MostUsedColors {get; private set;}
        public  List<int> ColorIncidences {get; private set;}
        private readonly ColorTable _colorTable ;
        public int NumGrayIntervals { get; set; }
        public int NumColorIntervals { get; set; }

        public DominantColor(int numRainbowColors, int numBwShades)
        {
            NumGrayIntervals = numBwShades;
            NumColorIntervals = numRainbowColors;

            _colorTable = new ColorTable(numRainbowColors, numBwShades);
   
        }

        public IEnumerable<RangeColor> AvailableColors {
            get
            {
                return _colorTable != null? _colorTable.Colors: new List<RangeColor>();
            }
        }


        public static IEnumerable<Color> GetGradients(Color start, Color end, int steps)
        {
            int stepA = ((end.A - start.A) / (steps - 1));
            int stepR = ((end.R - start.R) / (steps - 1));
            int stepG = ((end.G - start.G) / (steps - 1));
            int stepB = ((end.B - start.B) / (steps - 1));

            for (int i = 0; i < steps; i++)
            {
                yield return Color.FromArgb(start.A + (stepA * i),
                                            start.R + (stepR * i),
                                            start.G + (stepG * i),
                                            start.B + (stepB * i));
            }
        }

        /// <summary>
        ///  Get the the visible color spectrum (rainbow color) within the interval [0..1],
        ///  
        /// </summary>
        /// <param name="numRainbowColors">number of color in rainbow spectrum, bigger the value smaller the intervals </param>
        /// <param name="alpha">Alpha transparency value of the rainbow [0..255]</param>
        /// <returns></returns>
        public static IEnumerable<Color> GetRainbowColors(double numRainbowColors, int alpha = 255)
        {
            var returnlist = new  List<Color>();
            var step = 1/numRainbowColors;

            for (var i = 0d; i < 1 ; i += step)
            {
                returnlist.Add(GetColorforStep(i,alpha));
            }
            return returnlist;

        }

        /// <summary>
        ///  Get the the visible color spectrum (rainbow color) within the specific interval ,
        ///  
        /// </summary>
        /// <param name="numRainbowColors">number of color in rainbow spectrum, bigger the value smaller the intervals </param>
        /// <param name="alpha">Alpha transparency value of the rainbow [0..255]</param>
        /// <returns></returns>
        public static IEnumerable<Color> GetRainbowColors(double numRainbowColors, int interval, int alpha = 255)
        {
            var returnlist = new List<Color>();
            var step = 1 / numRainbowColors;
            double startVal = step * interval ;

            for (var i = startVal; i < 1; i += step)
            {
                returnlist.Add(GetColorforStep(i, alpha));
            }
            return returnlist;

        }

        /// <summary>
        ///  Get a specific color in the spectrum for a specific setp in interval [0..1]
        /// </summary>
        /// <param name="step">Step value [0..1]</param>
        /// <param name="alpha">Alpha transparency value of the rainbow [0.255]</param>
        /// <returns>Color of the step</returns>
        private static Color GetColorforStep( double step, int alpha=255)
        {
            // for value == 1 seems equal to 0 so we need to reduce it a bit
            step = Math.Round(step, 4).Equals(1.0) ? 0.99999 : step;

            var div = (Math.Abs(step%1)*6);
            var ascending = (int) ((div%1)*255);
            var descending = 255 - @ascending;

            switch ((int) div)
            {
                case 0:
                    return Color.FromArgb(alpha, 255, @ascending, 0);
                case 1:
                    return Color.FromArgb(alpha, @descending, 255, 0);
                case 2:
                    return Color.FromArgb(alpha, 0, 255, @ascending);
                case 3:
                    return Color.FromArgb(alpha, 0, @descending, 255);
                case 4:
                    return Color.FromArgb(alpha, @ascending, 0, 255);
                default: // case 5:
                    return Color.FromArgb(alpha, 255, 0, @descending);
            }
        }

        /// <summary>
        /// Calvulates the most used colors and assign the result to the MosteUsedColors List
        /// </summary>
        /// <param name="imageToProcess">The bitmap to process (For better results and faster processing on images bigger than 1000x1000 use thumbnails of 1/4 of the size)</param>
        /// <param name="step">Step value to the rainbow spectrum [0..1]</param>
        /// <param name="grayScaleSteps">Number of Grayscale steps </param>
        /// <returns>The list of colors in </returns>
        public  List<RangeColor> GetMostUsedColor(Bitmap imageToProcess)
        {
            var colorIncidences = new Dictionary<int, int>();

            MostUsedColors = new List<Color>();
            ColorIncidences = new List<int>();
            int totalPixels = 0;
            unsafe
            {

                BitmapData bitmapData =
                    imageToProcess.LockBits(new Rectangle(0, 0, imageToProcess.Width, imageToProcess.Height),
                        ImageLockMode.ReadWrite, imageToProcess.PixelFormat);

                int depth = Image.GetPixelFormatSize(imageToProcess.PixelFormat);
                int bytesPerPixel = depth/8;
                int heightInPixels = bitmapData.Height;
                int widthInPixels = bitmapData.Width;
                 totalPixels = widthInPixels*heightInPixels;

                int widthInBytes = bitmapData.Width*bytesPerPixel;
                byte* ptrFirstPixel = (byte*) bitmapData.Scan0;

                Parallel.For(0, heightInPixels,
                    () => new Dictionary<int, int>(),
                    (int y, ParallelLoopState state, Dictionary<int, int> localIncidents) =>
                    {
                        byte* currentLine = ptrFirstPixel + (y*bitmapData.Stride);
                        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                        {
                            var b = currentLine[x];
                            var g = 0;
                            var r = 0;
                            var a = 0;

                            var pixelColor = int.MinValue;
                            var color = Color.White;

                            if (depth == 32) // For 32 bpp get Red, Green, Blue and Alpha
                            {
                                g = currentLine[x + 1];
                                r = currentLine[x + 2];
                                a = currentLine[x + 3]; // alpha channel
                                color = Color.FromArgb(a, r, g, b);
                            }
                            else if (depth == 24) // For 24 bpp get Red, Green and Blue
                            {
                                g = currentLine[x + 1];
                                r = currentLine[x + 2];
                                color = Color.FromArgb(r, g, b);
                            }
                            else if (depth == 8) // For 8 bpp (Red, Green and Blue values are the same)
                            {
                                color = Color.FromArgb(b, b, b);
                            }

                            pixelColor = color.ToArgb();

                            if (localIncidents.Keys.Contains(pixelColor))
                            {
                                localIncidents[pixelColor]++;
                            }
                            else
                            {
                                localIncidents.Add(pixelColor, 1);
                            }

                        }
                        return localIncidents;
                    },
                    (Dictionary<int, int> localIncidents) =>
                    {
                        // Accessing shared variable; synchronize access.
                        lock (colorIncidences)
                        {
                            foreach (var localIncident in localIncidents)
                            {
                                if (!colorIncidences.ContainsKey(localIncident.Key))
                                {
                                    colorIncidences.Add(localIncident.Key, localIncident.Value);
                                }
                                else
                                {
                                    colorIncidences[localIncident.Key] += localIncident.Value;
                                }
                            }
                        }
                    }
                    );
                
                imageToProcess.UnlockBits(bitmapData);

                _colorTable.Reset(totalPixels);

                colorIncidences.ToList().ForEach(ci => _colorTable.UseColor(Color.FromArgb(ci.Key), ci.Value));
                
                foreach (var cr in _colorTable.Colors)
                {
                    MostUsedColors.Add(cr.MaxColor);
                    ColorIncidences.Add(cr.PixelCount);
                }
            }

            if (ContainsColor())
                return _colorTable.Colors.Where(c => c.PixelCount > 0 && c.IntervalId >= NumGrayIntervals && c.RangeColorPercentage > 1.0f).OrderByDescending(c => c.PixelCount).ToList();
           
          return _colorTable.Colors.Where(c => c.PixelCount > 0 &&  c.RangeColorPercentage  >1).OrderByDescending(c => c.PixelCount).ToList();
        }


        public int GetColorInterval(Color color)
        {
            return _colorTable.GetInterval(color);

           
        }

        public bool ContainsColor()
        {
            return _colorTable.Colors.Any(c=>c.IntervalId >= NumGrayIntervals & c.PixelCount>0 && c.RangeColorPercentage > 1.0f);


        }
    }
}
   