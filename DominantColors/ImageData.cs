using System.Collections.Generic;
using System.Drawing;

namespace TSoft.DominantColors
{
    public class ImageData  
    {
        public Color Color { get; set; }
        public bool IsBlackAndWhite { get; set; }
        public List<Color> DominantColors{ get; set; }
    }
}