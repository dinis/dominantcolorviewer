using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;

namespace TSoft.DominantColors
{
    /// <summary>
    /// 
    /// 
    /// </summary>
    public class ColorTable
    {
        public List<RangeColor> Colors { get; set; }
        public ColorTable()
        {
            CreateColorTable(22, 5);
        }

        public ColorTable(int rainbowNumIntervals, int numGrays)
        {
            CreateColorTable(rainbowNumIntervals, numGrays);
        }

        public void CreateColorTable(int rainbowNumIntervals, int numGrays)
        {
            Colors= new List<RangeColor>();
            Color lastColor = Color.Transparent;
            var id = 0;
 
            // shades of gray
            var colors = DominantColor.GetGradients(Color.Black, Color.White, numGrays+1).ToList();
            lastColor = Color.Black;
            for (var i = 1;i < colors.Count();i++)
            {
                var currColor = colors[i];
                if (i+1 < colors.Count)
                {
                    currColor = Color.FromArgb(currColor.A, 
                        currColor.R == 0 ? currColor.R : currColor.R - 1,
                        currColor.G == 0 ? currColor.G : currColor.G - 1,
                        currColor.B == 0 ? currColor.B : currColor.B - 1);
                }
                Colors.Add(new RangeColor(id++, lastColor, currColor));
                lastColor = currColor;               
            }
          
            // create rainbow
            colors = DominantColor.GetRainbowColors(rainbowNumIntervals).ToList();
            lastColor = colors.FirstOrDefault();
            for (var i = 1; i < colors.Count; i ++)
            {
                var currColor = colors[i];
           
                if (i + 1 < colors.Count)
                {
                    currColor = Color.FromArgb(currColor.A,currColor.R ,currColor.G, currColor.B == 0 ? currColor.B : currColor.B - 1);
                }
                Colors.Add(new RangeColor(id++, lastColor, currColor));

                lastColor =colors[i];             
            }
        }

        internal bool UseColor(Color color, int pixelCount)
        {
            var colorAux = Colors.FirstOrDefault(c => c.IsInRange(color));
            if (colorAux != null)
            {
                //colorAux.Used = true;
                //colorAux.ColorInUse = color;
                colorAux.PixelCount += pixelCount;
                return true;
            }

            return false;
        }



        internal void Reset(int totalPixelsInImage=0)
        {

            foreach (var rangeColor in Colors)
            {
                rangeColor.TotalPixels = totalPixelsInImage;
                //rangeColor.Used = false;
                //rangeColor.ColorInUse = Color.Transparent;
                rangeColor.PixelCount = 0;
                

            }
        }

        internal int GetInterval(Color color)
        {
            var colorAux = Colors.FirstOrDefault(c => c.IsInRange(color));

            return colorAux != null ? colorAux.IntervalId : -1;
        }
    }
}