using System;
using System.Drawing;

namespace TSoft.DominantColors
{
    public class RangeColor
    {
        public float Min { get; set; }
        public float Max { get; set; }
        public bool IsBlackAndWhite  { get; set; }
        public Color MinColor { get; set; }
        public Color MaxColor { get; set; }
  
     
        private const float BrightnessTolerance = 0.2f;
        private const float SaturationTolerance = 0.2f;
        
        public int IntervalId { get; set; }
        public int TotalPixels { get; set; }
        public int PixelCount { get; set; }
      

        public RangeColor(int id, Color minColor, Color maxColor)
        {   
            IntervalId = id;
            
            MinColor = minColor;
            MaxColor = maxColor;
     
            IsBlackAndWhite = !(MaxColor.GetHue() > 0);

            if (IsBlackAndWhite)
            {
                Min = MinColor.GetBrightness();
                Max = MaxColor.GetBrightness() - 0.0000000001f;
            }
            else
            {
                Min = MinColor.GetHue();
                Max = MaxColor.GetHue();
            }
        }


   
        public double RangeColorPercentage
        {
            get
            {
                if (TotalPixels == 0)
                    return 0;

                return PixelCount  *100 / TotalPixels;
            }
        }


        public bool IsInRange(Color color)
        {
            var inRange = false;

            var isBlackAndWhite = !(color.GetHue() > 0) || color.GetBrightness() < BrightnessTolerance || color.GetSaturation() < SaturationTolerance;

            if (isBlackAndWhite)
            {
                var brightness = color.GetBrightness();
                inRange = brightness >= Min && brightness <= Max;
            }
            else
            { 
                var hue = color.GetHue();
                inRange = hue >= Min && hue < Max;
              }

            return inRange;
        }




       
    }
}