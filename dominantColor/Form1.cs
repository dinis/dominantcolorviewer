﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using TSoft.DominantColors;

namespace DominantColorViewer
{
    public partial class Form1 : Form
    {
        Stopwatch sw = new Stopwatch();
        DominantColor dominantColorProcessor = new DominantColor(24,4);
       
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            var counter = 1;
            var w = 10;

            foreach (var color in dominantColorProcessor.AvailableColors)
            {
                var pn = new Panel()
                {
                    BackColor = color.MinColor,
                    Width = w,
                    Height = 44,
                    Left = ((int)(counter++ * w)),
                    Tag = color
                }; 
                 var pn1 = new Panel()
                {
                    BackColor = color.MaxColor,
                    Width = w,
                    Height = 44,
                    Left = ((int)(counter++ * w)),
                    Tag = color
                };
                var lbl = new Label()
                {
                    Text = color.IntervalId + "",
                    ForeColor = Color.White,
                    Width = 20,
                    Height = 11,
                    Top = 5,
                   
                    TextAlign = ContentAlignment.MiddleCenter
                };
                pn.Controls.Add(lbl);
                var lbl1 = new Label()
                {
                    Text = color.IntervalId + "",
                    ForeColor = Color.White,
                    Width = 20,
                    Height = 11,
                    Top = 5,
                 
                    TextAlign = ContentAlignment.MiddleCenter
                };
                pn1.Controls.Add(lbl1);

                pn.MouseEnter += pn_MouseHover; 
                pn1.MouseEnter += pn_MouseHover; 
                panel11.Controls.Add(pn);
                panel11.Controls.Add(pn1);

            }
        }
     
        
        private void button1_Click(object sender, EventArgs e)
        {
             
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                panel1.Controls.Clear();
                
                pictureBox1.ImageLocation = openFileDialog1.FileName;
                
                

                sw.Restart();
                var mostUsedColors = dominantColorProcessor.GetMostUsedColor(new Bitmap(openFileDialog1.FileName));
                sw.Stop();
                label1.Text = string.Format("Processed in {0} ms.",sw.ElapsedMilliseconds);

              
                var j = 0; 
                var t = 0;
                var l = 0;
                foreach (var usedColor in mostUsedColors)
                {
                   
                    var pn = new Panel()
                    {
                        BackColor = usedColor.MaxColor,
                        Width = 50,
                        Height = 50,
                        Left = l,
                        Top = t*50+5,
                        Tag = usedColor
                    };

                    var lbl = new Label()
                    {
                        Text = usedColor.IntervalId + "",
                        ForeColor = Color.Aqua,
                        Width = 25,
                        Height = 11,
                        Top = 5,
                        Left = 5,
                        TextAlign = ContentAlignment.MiddleCenter
                    };
                    pn.Controls.Add(lbl);

                    l += 55;
                    j++;

                    if (j == 10)
                    {
                        j = 0 ;
                        l = 0;
                        t++;
                    }
                    
                    pn.MouseEnter += pn_MouseHover;
                    panel1.Controls.Add(pn);
                    
                }
                
            }
        }

       

        void pn_MouseHover(object sender, EventArgs e)
        {
            var pnl = (sender as Panel);
            var panelColor = pnl.Tag as RangeColor;

            textBox1.Text =
                string.Format(
                    "Hue:{0:F10}\r\n\r\nBri: {1:F10}\r\n\r\nSat: {2:F10}\r\n\r\nARGB: {3:F10}, {4}, {5}, {6}\r\n\r\nMin: {7:F10}\r\n\r\nMax: {8:F10}\r\n\r\nValToArgb: {9:F10}\r\n\r\nBW:{10}\r\n\r\n{11}%\r\n\r\nCount:{12}",
                    pnl.BackColor.GetHue(),
                    pnl.BackColor.GetBrightness(),
                    pnl.BackColor.GetSaturation(),
                    pnl.BackColor.A,
                    pnl.BackColor.R,
                    pnl.BackColor.G,
                    pnl.BackColor.B,
                    panelColor != null ?panelColor.Min : 0, 
                    panelColor!= null ? panelColor.Max : 0,
                    pnl.BackColor.ToArgb(),
                    panelColor != null ? panelColor.IsBlackAndWhite.ToString() : "",
                       panelColor!= null ? panelColor.RangeColorPercentage : -1,
                       panelColor!= null ? panelColor.PixelCount : -1
                   );


        }

        private Bitmap _b;
        private int _x;
        private int _y;


        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            _x = e.X;
            _y = e.Y;


            txtInt.Text = "";
            txtHex.Text = "";

            txtPerc.Text = "";
            txtH.Text = "";
            txtB.Text = "";
            txtS.Text = "";


            if (pictureBox1.Image == null)
                return;

            if (_b == null)
                _b = new Bitmap(pictureBox1.Image);

            try
            {
                
                Color color = _b.GetPixel(_x, _y);
                panelColor.BackColor = color;
                    
                var id = dominantColorProcessor.GetColorInterval(color);

                if (id < 0)
                    return;

                var colorrange = dominantColorProcessor.AvailableColors.ToList()[id];

                txtInt.Text = colorrange.IntervalId.ToString();
                txtHex.Text = color.ToHexadecimal();

                txtPerc.Text = colorrange.RangeColorPercentage.ToString() + "%";
                txtH.Text = color.GetHue().ToString();
                txtB.Text = color.GetBrightness().ToString();
                txtS.Text = color.GetSaturation().ToString();
            }catch{}

        }
       
        
       
       
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
          
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
                 

        }

       

    }
}
